const std = @import("std");
const uefi = std.os.uefi;
const unicode = std.unicode;

pub fn main() void {
    const con_out = uefi.system_table.con_out.?;
    _ = con_out.clearScreen();
    _ = con_out.outputString(unicode.utf8ToUtf16LeStringLiteral("Hello, World!"));
    while (true) {}
}
