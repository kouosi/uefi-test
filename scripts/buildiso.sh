#!/bin/bash
set -e
TARGET=osdev.img
dd if=/dev/zero of=$TARGET bs=512 count=100352
sfdisk -q $TARGET < <(printf "label: gpt\n sector-size: 512\n type=uefi, bootable")
mkfs.fat -F 32 --offset=2048 -S 512 $TARGET 49152
mmd -i $TARGET@@1M ::/EFI
mmd -i $TARGET@@1M ::/EFI/BOOT
mcopy -i $TARGET@@1M ./zig-out/bin/loader.efi ::/EFI/BOOT/BOOTX64.EFI
