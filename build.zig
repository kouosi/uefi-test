const std = @import("std");
const ArrayList = std.ArrayList;
const CrossTarget = std.zig.CrossTarget;

const loader_target = CrossTarget{
    .cpu_arch = .x86_64,
    .os_tag = .uefi,
};

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{
        .default_target = loader_target,
    });

    const optimize = b.standardOptimizeOption(.{});

    const loader = b.addExecutable(.{
        .name = "loader",
        .root_source_file = b.path("src/main.zig"),
        .target = target,
        .optimize = optimize,
    });

    b.installArtifact(loader);

    // Generate iso image
    const gen_iso = b.addSystemCommand(&[_][]const u8{"./scripts/buildiso.sh"});
    gen_iso.step.dependOn(b.getInstallStep());

    // Run with qemu
    var qemu_args_list = ArrayList([]const u8).init(b.allocator);
    defer qemu_args_list.deinit();

    try qemu_args_list.append("qemu-system-x86_64");
    try qemu_args_list.append("-hda");
    try qemu_args_list.append("osdev.img");
    try qemu_args_list.append("-display");
    try qemu_args_list.append("gtk,gl=on");
    try qemu_args_list.append("-enable-kvm");
    try qemu_args_list.append("-bios");
    try qemu_args_list.append("OVMF.fd");

    const qemu_args = try qemu_args_list.toOwnedSlice();
    const qemu_cmd = b.addSystemCommand(qemu_args);
    qemu_cmd.step.dependOn(&gen_iso.step);

    var isogen_step = b.step("iso", "Build iso file");
    isogen_step.dependOn(&gen_iso.step);

    const run_step = b.step("run", "Run with qemu");
    run_step.dependOn(&qemu_cmd.step);
}
